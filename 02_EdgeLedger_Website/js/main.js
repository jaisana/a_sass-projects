const year = new Date().getFullYear();

document.querySelector('.year').innerHTML = year;

// Sticky menu background
window.addEventListener('scroll', function() {
  if (window.scrollY > 150) {
    document.querySelector('.main_nav').style.opacity = 0.9;
  } else {
    document.querySelector('.main_nav').style.opacity = 1;
  }
});

// Smooth Scrolling
$('.nav_link').on('click', function(event) {
  if (this.hash !== '') {
    event.preventDefault();

    const hash = this.hash;

    $('html, body').animate(
      {
        scrollTop: $(hash).offset().top - 100
      },
      800
    );
  }
});
